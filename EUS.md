# How to update Red Hat Enterprise Linux via Minor Releases and Extended Update Support

Customers often tell me that they need to stay on a specific RHEL minor release in order to maintain a supported configuration for a third party application, such as SAP for example.  It's also typically true for large, business critical applications that don't tolerate frequent downtime for updates.

This article explains the mechanisms available in Red Hat Enterprise Linux (RHEL) to help make this possible.

So here are some questions to think of:
- What is a RHEL minor release?
- How do we get updates, yet stay on a specific RHEL minor release?
- How do we use Extended Update Support so you can stay on a specific minor release for longer?

# What is a RHEL minor release?

Red Hat committed to a six monthly minor release cadence with RHEL 8, and at the time of writing RHEL 8.4 is the most recent release.  Each minor release is ultimately just a label for a specific set of packages, baselined, tested, and released at a certain time.

# Extended Update Support

Red Hat's Extended Update Support (EUS) exists so that you can remain on a specific RHEL minor release for an extended period of time, helping you stay with a supported configuration.  

A given minor release is only fully supported for 6 months, ie. until the next minor release comes out.  However, if the system has the Extended Update Support add-on available and is running an eligible EUS release, then further select updates to that minor version will be available.  The RHEL EUS release repository receives all security, bugfix, and enhancement errata until the next minor release is released. Afterwards, it only receives the selected backports as per the EUS inclusion policy.
 

# Updating to the very latest software

This is both the default, and simplest approach.  If you just want to update to the latest packages for your major RHEL release, then it's one simple command.  I am using RHEL 8.2 for these examples.

The initial state shows the RHEL 8.2 system not actually being tied to a specific release:
```
[root@bblasco82to84 ~]# subscription-manager release
Release not set

[root@bblasco82to84 ~]# cat /etc/redhat-release 
Red Hat Enterprise Linux release 8.2 (Ootpa)
```

Let's check the installed and available kernel updates.  Here you can see the installed RHEL 8.2 kernel, as well as a far newer 8.4 kernel available to be installed.
```
[root@bblasco82to84 ~]# dnf list kernel
Updating Subscription Management repositories.
This system is registered to Red Hat Subscription Management, but is not receiving updates. You can use subscription-manager to assign subscriptions.
Red Hat Enterprise Linux 8 for x86_64 - BaseOS (RPMs)                                                                      14 MB/s |  33 MB     00:02    
Red Hat Enterprise Linux 8 for x86_64 - AppStream (RPMs)                                                                   18 MB/s |  31 MB     00:01    
Installed Packages
kernel.x86_64                                              4.18.0-193.el8                                                    @anaconda                    
Available Packages
kernel.x86_64                                              4.18.0-305.7.1.el8_4                                              rhel-8-for-x86_64-baseos-rpms
```

Now let's run a full system update (quietly):
```
[root@bblasco82to84 ~]# dnf update -y --quiet
warning: /var/cache/dnf/rhel-8-for-x86_64-baseos-rpms-51b3b78d5698246b/packages/libsmbios-2.4.1-2.el8.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID fd431d51: NOKEY
Importing GPG key 0xFD431D51:
 Userid     : "Red Hat, Inc. (release key 2) <security@redhat.com>"
 Fingerprint: 567E 347A D004 4ADE 55BA 8A5F 199E 2F91 FD43 1D51
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
Importing GPG key 0xD4082792:
 Userid     : "Red Hat, Inc. (auxiliary key) <security@redhat.com>"
 Fingerprint: 6A6A A7C9 7C88 90AE C6AE BFE2 F76F 66C3 D408 2792
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release

```

The results show the system is updated to RHEL 8.4, with the latest update to the RHEL 8.4 kernel 4.18.0-305 activating on next boot:
```
[root@bblasco82to84 ~]# cat /etc/redhat-release
Red Hat Enterprise Linux release 8.4 (Ootpa)

[root@bblasco82to84 ~]# dnf list kernel
Updating Subscription Management repositories.
Last metadata expiration check: 0:07:26 ago on Wed 30 Jun 2021 08:54:29 EDT.
Installed Packages
kernel.x86_64                                             4.18.0-193.el8                                                    @anaconda                     
kernel.x86_64                                             4.18.0-305.7.1.el8_4                                              @rhel-8-for-x86_64-baseos-rpms
```

So, no matter which minor release you started with, this process will pull down the latest packages available for RHEL 8 and update you to the latest minor RHEL release.

# Sticking with a specific minor release

Firstly, you will need to tell the system that you are tying it to a minor release *as soon as you register it*, via the subscription-manager tool.  Here we will tie a different RHEL 8.2 system to stay on RHEL 8.2:

```
[root@bblasco82 ~]# subscription-manager register --release=8.2
Registering to: subscription.rhsm.redhat.com:443/subscription
Username: someuser
Password: 
The system has been registered with ID: 94474ba1-9164-4fba-bad8-6ce0bf42cc22
The registered system name is: bblasco82.lan

```

From here we check which release the system is running and which kernels are available.  In this instance you can see a newer RHEL 8.2 kernel available, but the system remains on the 4.18.0-193 track:

```
[root@bblasco82 ~]# subscription-manager release
Release: 8.2

[root@bblasco82 ~]# cat /etc/redhat-release 
Red Hat Enterprise Linux release 8.2 (Ootpa)

[root@bblasco82 ~]# uname -r
4.18.0-193.el8.x86_64

[root@bblasco82 ~]# dnf list kernel
Updating Subscription Management repositories.
This system is registered to Red Hat Subscription Management, but is not receiving updates. You can use subscription-manager to assign subscriptions.
Last metadata expiration check: 0:05:37 ago on Wed 30 Jun 2021 09:09:25 EDT.
Installed Packages
kernel.x86_64                                             4.18.0-193.el8                                                    @anaconda                     
Available Packages
kernel.x86_64                                             4.18.0-193.28.1.el8_2                                             @rhel-8-for-x86_64-baseos-rpms

```

Run a system update (quietly):
```
[root@bblasco82 ~]# dnf update -y --quiet
warning: /var/cache/dnf/rhel-8-for-x86_64-baseos-rpms-eca8c541051c10da/packages/linux-firmware-20191202-97.gite8a0f4c9.el8.noarch.rpm: Header V3 RSA/SHA256 Signature, key ID fd431d51: NOKEY
Importing GPG key 0xFD431D51:
 Userid     : "Red Hat, Inc. (release key 2) <security@redhat.com>"
 Fingerprint: 567E 347A D004 4ADE 55BA 8A5F 199E 2F91 FD43 1D51
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
Importing GPG key 0xD4082792:
 Userid     : "Red Hat, Inc. (auxiliary key) <security@redhat.com>"
 Fingerprint: 6A6A A7C9 7C88 90AE C6AE BFE2 F76F 66C3 D408 2792
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release

```

Results:
```
[root@bblasco82 ~]# cat /etc/redhat-release 
Red Hat Enterprise Linux release 8.2 (Ootpa)

[root@bblasco82 ~]# dnf list kernel
Updating Subscription Management repositories.
This system is registered to Red Hat Subscription Management, but is not receiving updates. You can use subscription-manager to assign subscriptions.
Last metadata expiration check: 0:10:37 ago on Wed 30 Jun 2021 09:09:25 EDT.
Installed Packages
kernel.x86_64                                             4.18.0-193.el8                                                    @anaconda                     
kernel.x86_64                                             4.18.0-193.28.1.el8_2                                             @rhel-8-for-x86_64-baseos-rpms

```

As you can see, running an update will only update packages within that minor release.  In this instance, there was a minor update to the kernel package, from 4.18.0-193.el8, but patched to 4.18.0.193.28.1.el8_2

# Using EUS to stick with a minor release for longer

To access the EUS repositories (and therefore packages) for RHEL 8.2, we would need to do the following to our RHEL 8.2 system:

Check the enabled repositories:
```
[root@bblasco82 ~]# subscription-manager repos --list-enabled
+----------------------------------------------------------+
    Available Repositories in /etc/yum.repos.d/redhat.repo
+----------------------------------------------------------+
Repo ID:   rhel-8-for-x86_64-baseos-rpms
Repo Name: Red Hat Enterprise Linux 8 for x86_64 - BaseOS (RPMs)
Repo URL:  https://cdn.redhat.com/content/dist/rhel8/8.2/x86_64/baseos/os
Enabled:   1

Repo ID:   rhel-8-for-x86_64-appstream-rpms
Repo Name: Red Hat Enterprise Linux 8 for x86_64 - AppStream (RPMs)
Repo URL:  https://cdn.redhat.com/content/dist/rhel8/8.2/x86_64/appstream/os
Enabled:   1
```

Let's enable the EUS repos:
```
[root@bblasco82 ~]# subscription-manager repos --enable rhel-8-for-x86_64-baseos-eus-rpms
Repository 'rhel-8-for-x86_64-baseos-eus-rpms' is enabled for this system.
[root@bblasco82 ~]# 
[root@bblasco82 ~]# 
[root@bblasco82 ~]# subscription-manager repos --enable rhel-8-for-x86_64-appstream-eus-rpms
Repository 'rhel-8-for-x86_64-appstream-eus-rpms' is enabled for this system.
```

Looking at the kernels are available now, you can see an even newer patch available via the EUS repository:
```
[root@bblasco82 ~]# dnf list kernel
Updating Subscription Management repositories.
This system is registered to Red Hat Subscription Management, but is not receiving updates. You can use subscription-manager to assign subscriptions.
Red Hat Enterprise Linux 8 for x86_64 - BaseOS - Extended Update Support (RPMs)                                            14 MB/s |  29 MB     00:02    
Last metadata expiration check: 0:00:01 ago on Wed 30 Jun 2021 09:34:34 EDT.
Installed Packages
kernel.x86_64                                           4.18.0-193.el8                                                   @anaconda                        
kernel.x86_64                                           4.18.0-193.28.1.el8_2                                            @rhel-8-for-x86_64-baseos-rpms   
Available Packages
kernel.x86_64                                           4.18.0-193.56.1.el8_2                                            rhel-8-for-x86_64-baseos-eus-rpms
```

Run a system update:
```
[root@bblasco82 ~]# dnf update -y --quiet
[root@bblasco82 ~]# 
```

Results:
```
[root@bblasco82 ~]# cat /etc/redhat-release 
Red Hat Enterprise Linux release 8.2 (Ootpa)

[root@bblasco82 ~]# dnf list kernel
Updating Subscription Management repositories.
This system is registered to Red Hat Subscription Management, but is not receiving updates. You can use subscription-manager to assign subscriptions.
Last metadata expiration check: 0:06:07 ago on Wed 30 Jun 2021 09:34:34 EDT.
Installed Packages
kernel.x86_64                                           4.18.0-193.el8                                                  @anaconda                         
kernel.x86_64                                           4.18.0-193.28.1.el8_2                                           @rhel-8-for-x86_64-baseos-rpms    
kernel.x86_64                                           4.18.0-193.56.1.el8_2                                           @rhel-8-for-x86_64-baseos-eus-rpms
```

Here we can see that the update would allowed the system to consume the EUS update packages for the minor version, and we installed kernel update 4.18.0-193.56.1.el8_2.  This update would contain backports of specific changes from newer upstream kernel versions.

# Wrapping up

These examples show the mechanisms we have available to finely control which release of RHEL we allow a system to update to, which helps us maintain a supported configuration for the workloads we run.

As always, be sure to test out any of what you have learned here on a non-production system first to ensure you get the results you expect, before pushing changes into production.  

Happy updating!

# Useful references

How to display only the enabled repos on a system using subscription-manager 
https://access.redhat.com/solutions/1985803

Reference: How to tie a system to a specific update of Red Hat Enterprise Linux? 
https://access.redhat.com/solutions/238533

Red Hat Enterprise Linux (RHEL) Extended Update Support (EUS) Overview 
https://access.redhat.com/articles/rhel-eus
